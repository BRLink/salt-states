/scripts/tools:
 file.recurse:
 - source: salt://scripts-base/files/tools
 - include_empty: True
 - clean: True
 - file_mode: 755
 - dir_mode: 755
