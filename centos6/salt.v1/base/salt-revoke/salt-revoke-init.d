#!/bin/bash
#
# salt-revoke      Este script executa o revoke de chaves dos masters 
#
# chkconfig: - 60 50

MASTER_FILE=/etc/salt/minion.d/masters.conf

start() {
	echo "Iniciando salt-revoke"
	touch /var/lock/subsys/salt-revoke
#        return 1
}

stop() {
	echo "Desativando salt-revoke"
	MASTERS=`cat $MASTER_FILE | grep '-' | awk '{print $2}'`
	for master in $MASTERS; do
	    while :
	    do
		RETURN=`salt-call saltutil.revoke_auth --master $master`
		VALIDA=`echo $RETURN | grep local`
		if [ "$VALIDA" ]; then
			break
		fi
		sleep $[ ( $RANDOM % 10 )  + 1 ]s
            done
	done
	rm -f  /var/lock/subsys/salt-revoke
        return 1
}

# See how we were called.
case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart|reload)
        stop
        start
        RETVAL=$?
        ;;
  condrestart)
        if [ -f /var/lock/subsys/$prog ]; then
            stop
            start
            RETVAL=$?
        fi
        ;;
  status)
        status $prog
        RETVAL=$?
        ;;
  *)
        echo $"Usage: $0 {start|stop|restart|condrestart|status}"
        exit 2
esac

exit $RETVAL
