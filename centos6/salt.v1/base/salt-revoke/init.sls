##
# Script responsavel por remover as chaves 
# do minion nos seus masters, no momento do shutdown
##

# copia o script do init
/etc/init.d/salt-revoke:
  file.managed:
    - source:
      - salt://salt-revoke/salt-revoke-init.d
    - mode: 755

# Manter service ativo (cria S em rc3.d)
salt-revoke:
  service.enabled

# Desativar servico nos runlevel 6 e 0 (cria K em rc6.d e rc0.d)
chkconfig off salt-revoke:
  cmd.run:
    - name: chkconfig --level 06 salt-revoke off
    - cwd: /

# Ativa servico para criar arquivo de lock
Start salt-revoke:
  cmd.run:
    - name: /etc/init.d/salt-revoke start
    - cwd: /

