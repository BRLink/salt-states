zabbix-repo-pkg:
  pkg.installed:
    - sources:
      - zabbix-repo: salt://zabbix-proxy/files/zabbix-release-2.2-1.el6.noarch.rpm

zabbix packages:
  pkg.installed:
    - pkgs:
      - zabbix-proxy 
      - zabbix-proxy-mysql 
      
/etc/zabbix/zabbix_agentd.conf:
  file.managed:
    - source:
      - salt://zabbix-proxy/files/zabbix_proxy.conf
    - mode: 644

#Ativa servico Zabbix Proxy na inicializacao
chkconfig zabbix-proxy:
  cmd.run:
    - name: chkconfig zabbix-proxy on
    - cwd: /
