zabbix-repo-pkg:
  pkg.installed:
    - sources:
      - zabbix-repo: salt://zabbix-agent/files/zabbix-release-2.2-1.el6.noarch.rpm

zabbix packages:
  pkg.installed:
    - pkgs:
      - zabbix-agent 
      
/etc/zabbix/zabbix_agentd.conf:
  file.managed:
    - source:
      - salt://zabbix-agent/files/zabbix_agentd.conf
    - mode: 644

#Ativa servico Zabbix Agent na inicializacao
chkconfig zabbix-agent:
  cmd.run:
    - name: chkconfig zabbix-agent on
    - cwd: /

