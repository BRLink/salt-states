/scripts/tools/format-ephemeral.sh:
  file.managed:
    - source:
      - salt://mount-ephemeral/format-ephemeral.sh
    - mode: 755

/scripts/tools/format-ephemeral.sh /dev/xvdf:
  cmd.run

/mnt/md0:
  mount.mounted:
    - device: /dev/xvdf
    - fstype: xfs
    - mkmnt: True
    - opts:
      - defaults,sunit=128,swidth=512,logbufs=8,logbsize=256k,noatime,nodiratime

