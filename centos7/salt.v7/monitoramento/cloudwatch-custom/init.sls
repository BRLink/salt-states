#pacotes
cloudwatch packages:
  pkg.installed:
    - pkgs:
      - perl-DateTime 
      - perl-Sys-Syslog 
      - perl-LWP-Protocol-https

# scripts
/scripts/tools/aws-scripts-mon:
  file.recurse:
    - source: salt://cloudwatch-custom/files/aws-scripts-mon
    - include_empty: True
    - file_mode: 755
    - dir_mode: 755

# cronfile
/etc/cron.d/cloudwatch.cron:
  file.managed:
    - source:
      - salt://cloudwatch-custom/files/cloudwatch.cron
    - mode: 644


