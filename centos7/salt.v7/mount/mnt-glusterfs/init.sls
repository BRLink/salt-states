/media:
  mount.mounted:
    - device: glusterfs1.infra.cliente.com.br:/vol-01
    - name: /media
    - fstype: glusterfs
    - mkmnt: True
    - opts:
       - defaults
       - backupvolfile-server=glusterfs2.infra.cliente.com.br
